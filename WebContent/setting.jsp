<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザー編集画面</title>
	</head>
	<body>
		<div class="main-contents">
			<a href = "management">ユーザー管理画面</a>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<form action="setting" method="post"><br />
            <label for="account">アカウント</label>
            <input name="account" id="account" value="${user.account}" > <br />
			<br>
            <label for="password">パスワード</label>
            <input name="password" type="password" id="password" /> <br />
			<br>
            <label for="confirmPassword">確認用パスワード</label>
            <input name="confirmPassword" type="password" id="confirmPassword" /> <br />
			<br>
            <label for="name">名前</label>
            <input name="name" id="name" value="${user.name}" />
            <br>
            <br>
            <c:if test="${user.id != loginUser.id}">
    		<span>支社</span>
            <select name="branchId">
				<c:forEach items="${branches}" var="branch">
					<c:if test="${user.branchId == branch.id }"><option value="${branch.id}" selected><c:out value="${branch.name}"></c:out></option></c:if>
					<c:if test="${user.branchId != branch.id }"><option value="${branch.id}"><c:out value="${branch.name}"></c:out></option></c:if>
				</c:forEach>
			</select>
			<br>
			<br>
            <span>部署</span>
            <select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<c:if test="${user.departmentId == department.id}"><option value="${department.id}" selected><c:out value="${department.name}"></c:out></option></c:if>
					<c:if test="${user.departmentId != department.id}"><option value="${department.id}"><c:out value="${department.name}"></c:out></option></c:if>
				</c:forEach>
			</select>
			</c:if>
			<c:if test="${user.id == loginUser.id}">
    			<input name="branchId" type="hidden" value="${user.branchId}">
               	<input name="departmentId" type="hidden" value="${user.departmentId}">
			</c:if>
			<br>
			<br>
			<input name="userId" type="hidden" value="${user.id}">
            <input type="submit" value="更新" /> <br />
            <br>
        </form>
		</div>
            <div class="copyright">Copyright(c)Kaito Uchida</div>
	</body>
</html>