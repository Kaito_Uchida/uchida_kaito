<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>ユーザー管理画面</title>
	<script type="text/javascript">
		function dispStop(){
			if(window.confirm("このユーザーを停止しますか？")){
				return true;
			}else{
				window.alert("キャンセルしました");
				return false
			}
		}
		function dispRevive(){
			if(window.confirm("このユーザーを復活しますか？")){
				return true;
			}else{
				window.alert("キャンセルしました");
				return false
			}
		}
		</script>
	</head>
	<body>
	<div class="link">
		<a href="./">ホーム</a> <a href="signup">ユーザー新規登録</a>
	</div>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>
	<div class="users">
		<c:if test="${not empty users}">
			<c:forEach items="${users}" var="user">
				<div class="user-contents">
					<div class="account">
						アカウント :
						<c:out value="${user.account}"></c:out>
					</div>
					<div class="name">
						名前 :
						<c:out value="${user.name}"></c:out>
					</div>
					<div class="branchName">
						支社 :
						<c:out value="${user.branchName}"></c:out>
					</div>
					<div class="departmentName">
						部署 :
						<c:out value="${user.departmentName}"></c:out>
					</div>
					<div class="status">
						アカウント復活停止状態
						<c:if test="${user.isStopped == 0}">稼働中</c:if>
						<c:if test="${user.isStopped == 1}">停止中</c:if>
					</div>
				</div>
				<form action="setting" method="get">
					<input name="userId" id="userId" type="hidden" value="${user.id}">
					<input type="submit" value="編集" />
				</form>
				<br>
				<c:if test="${user.isStopped == 0 && user.id != loginUser.id}">
					<form action="stop" method="post" onsubmit="return dispStop()">
						<input type="submit" value="停止">
						<input name="userId" type="hidden" value="${user.id}">
						<input name="currentStatus" type="hidden" value=1>
					</form>
				</c:if>
				<c:if test="${user.isStopped == 1 && user.id != loginUser.id}">
					<form action="stop" method="post" onsubmit="return dispRevive()">
						<input type="submit" value="復活">
						<input name="userId" type="hidden" value="${user.id}">
						<input name="currentStatus" type="hidden" value=0>
					</form>
				</c:if>
				<br>
			</c:forEach>
		</c:if>
	</div>
	<div class="copyright">Copyright(c)Kaito Uchida</div>
</body>
</html>