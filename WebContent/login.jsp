<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>ログイン</title>
	</head>
	<body>
	<div class="main contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="login" method="post"><br />
		<label for="account">アカウント</label>
		<input name="account" id="account" value="${account}"/> <br />
		<label	for="password">パスワード</label>
		<input name="password" type="password"id="password" value="${password}"/> <br />
		<input type="submit" value="ログイン" /> <br />
		</form>
	</div>
	<div class="copyright">Copyright(c)Kaito Uchida</div>
</body>
</html>