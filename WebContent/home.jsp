<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>ホーム画面</title>
		<script type="text/javascript">
		function disp(){
			if(window.confirm("本当に削除しますか？")){
				return true;
			}else{
				window.alert("キャンセルしました");
				return false
			}
		}
		</script>
	</head>
	<body>
	<header>
		<div class="header-content">
			<a href="message">新規投稿</a> <c:if test="${loginUser.departmentId == 1}"><a href="management">ユーザー管理</a></c:if> <a href="logout">ログアウト</a>
		</div>
	</header>

	<div class="messages"><br/>
		投稿一覧<br/>
		<br/>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="index.jsp">
			<input type="date" name="startDate" id="startDate" value="${startDate}"> ～ <input type="date" name="endDate" id="endDate" value="${endDate}"><br>
			<input name="search" id="search" value="${searchWord}">
			<input type="submit" value="絞り込み">
		</form><br>
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="account-name">
					アカウント :
					<span class="account">
						<c:out value="${message.account}" />
					</span>
				</div>
				<div class="title">
					件名 :
					<c:out value="${message.title}" />
				</div>
				<div class="category">
					カテゴリー :
					<c:out value="${message.category}" />
				</div>
				<div class="text">
					投稿内容 :
					<c:forEach items="${fn:split(message.text ,'
')}" var="messageText">
						<c:out value="${messageText}"></c:out><br>
					</c:forEach>
				</div>
				<div class="date">
					投稿日時 :
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
				<c:if test="${not empty loginUser}">
					<c:if test="${message.userId == loginUser.id }">
						<form action="deleteMessage" method="post" onsubmit="return disp()">
							<input name="deleteMessageId" id="deleteMessageId" type="hidden" value="${message.id}">
							<input type="submit" value="削除">
						</form>
					</c:if>
				</c:if>
				<br /> コメント一覧<br />
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId == message.id}">
						<div class="comments">
							<div class="commentUserName">
								ユーザー名 :
								<c:out value="${comment.account}" />
							</div>

							<div class="commentText">
								コメント内容 :
								<c:forEach items="${fn:split(comment.text, '
')}" var="str">
									<div><c:out value="${str}"></c:out></div>
								</c:forEach>
							</div>

							<div class="commentDate">
								コメント日時 :
								<c:out value="${comment.createdDate}" />
							</div>
						</div>
						<br>
						<c:if test="${not empty loginUser}">
							<c:if test="${comment.userId == loginUser.id }">
								<form action="deleteComment" method="post"onsubmit="return disp()">
									<input name="deleteCommentId" id="deleteCommentId" type="hidden"value="${comment.id}">
									<input type="submit" value="削除"><br />
								</form>
							</c:if>
						</c:if>
					</c:if>
				</c:forEach>
				<div class="commentForm">
					<form action="comment" method="post">
						<textarea name="text" cols="50" rows="5" id="text" placeholder="コメントを投稿できます"></textarea>
						<input name="messageId" id="messageId" type="hidden"value="${message.id}"><br />
						<input type="submit" value="コメント投稿" /> <br />
					</form>
				</div>
			</div>
			<br/>
		</c:forEach>
	</div>
	<div class="copyright">Copyright(c)Kaito Uchida</div>
</body>
</html>