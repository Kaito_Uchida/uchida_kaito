<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>新規投稿画面</title>
	</head>
	<body>
		<div class="main-contents">
		<a href="./">ホーム</a>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="message" method="post"><br />
			<label for="title">件名</label> <input name="title" id="title" value="${message.title}" /><br />
			<label for="category">カテゴリ</label> <input name="category"id="category" value="${message.category}" /> <br />
			投稿内容
			<textarea name="text" cols="100" rows="5" id="text">${message.text}</textarea>

			<input type="submit" value="投稿" /> <br />
		</form>
	</div>
	</body>
</html>