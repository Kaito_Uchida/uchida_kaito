package uchida_kaito.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uchida_kaito.service.UserService;

@WebServlet(urlPatterns = { "/stop" })

public class StopServlet  extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int stoppedUserId = Integer.parseInt(request.getParameter("userId"));
		int currentStatus = Integer.parseInt(request.getParameter("currentStatus"));
		new UserService().stop(stoppedUserId, currentStatus);
		response.sendRedirect("management");
	}
}
