package uchida_kaito.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uchida_kaito.beans.User;
import uchida_kaito.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        User user = new UserService().select(account, password);
        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        if(StringUtils.isEmpty(account) && StringUtils.isEmpty(password)) {
        	errorMessages.add("アカウントを入力してください");
        	errorMessages.add("パスワードを入力してください");
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        else if((user == null) || (user.getIsStopped() == 1)) {
            errorMessages.add("アカウントまたはパスワードが誤っています");
            request.setAttribute("errorMessages", errorMessages);
            session.setAttribute("account", account);
            session.setAttribute("password", password);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }
}