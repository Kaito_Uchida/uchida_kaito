package uchida_kaito.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uchida_kaito.beans.User;
import uchida_kaito.beans.UserComment;
import uchida_kaito.beans.UserMessage;
import uchida_kaito.service.CommentService;
import uchida_kaito.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })

public class HomeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
		User user = (User) request.getSession().getAttribute("loginUser");

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String searchWord = request.getParameter("search");

		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("searchWord", searchWord);

		List<UserMessage> messages = new MessageService().select(startDate, endDate, searchWord);
		List<UserComment> comments = new CommentService().select();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

}