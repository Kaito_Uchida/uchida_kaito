package uchida_kaito.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uchida_kaito.beans.Comment;
import uchida_kaito.beans.User;
import uchida_kaito.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })

public class CommentServlet extends HttpServlet{

	 @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws IOException, ServletException {

	        HttpSession session = request.getSession();
	        List<String> errorMessages = new ArrayList<String>();

	        String text = request.getParameter("text");
	        int messageId  = Integer.parseInt(request.getParameter("messageId"));


	        if (!isValid(text, errorMessages)) {
	            session.setAttribute("errorMessages", errorMessages);
	            response.sendRedirect("./");
	            return;
	        }

	        Comment comment = new Comment();
	        User user = (User) session.getAttribute("loginUser");

	        comment.setText(text);
	        comment.setUserId(user.getId());
	        comment.setMessageId(messageId);

	        new CommentService().insert(comment);
	        response.sendRedirect("./");
	    }

	 private boolean isValid(String text ,List<String> errorMessages) {
		 if (StringUtils.isBlank(text)) {
			 errorMessages.add("コメントを入力してください");
		 }else if (text.length() > 500) {
			errorMessages.add("本文は500文字以内で入力してください");
		 }
		 if (errorMessages.size() != 0) {
			 return false;
		 }
		 return true;
	 }
}
