package uchida_kaito.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uchida_kaito.beans.Branch;
import uchida_kaito.beans.Department;
import uchida_kaito.beans.User;
import uchida_kaito.exception.NoRowsUpdatedRuntimeException;
import uchida_kaito.service.BranchService;
import uchida_kaito.service.DepartmentService;
import uchida_kaito.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet  extends HttpServlet {
	List<Branch> branches = new BranchService().select();
	List<Department> departments = new DepartmentService().select();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		User validationUser = new UserService().settingSelect(request.getParameter("userId"));

		if (validationUser == null) {
			errorMessages.add("不正なパラメーターが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("/management").forward(request, response);
			return;
		}else {

			request.setAttribute("user", validationUser);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		String confirmPassword = request.getParameter("confirmPassword");

		if (isValid(user, errorMessages, confirmPassword)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}
		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("userId")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages, String confirmPassword) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();
		String accountRegex ="^[A-Za-z0-9]{6,20}$";
		String passwordRegex = "^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}$";

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if  (!account.matches(accountRegex)) {
			errorMessages.add("アカウント名は半角英数字6文字以上20文字以下で入力してください");
		} else if(account.length() < 6) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		}else if(account.length() > 20) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		} else {
			User updatedUser = new UserService().select(account);
			if( updatedUser != null && user.getId() != updatedUser.getId()) {
				errorMessages.add("このアカウント名は既に使用されています");
			}
		}

		if(!StringUtils.isEmpty(password) && !password.matches(passwordRegex)) {
			errorMessages.add("パスワードは半角記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
		}

		if(!StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if(password.length() < 6) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		} else if(password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		} else if(!StringUtils.isEmpty(password) && !password.matches(confirmPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		if(StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");
		} else if (name.length() > 10) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if((branchId == 1 && departmentId > 2) || (branchId != 1 && departmentId < 3)) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
