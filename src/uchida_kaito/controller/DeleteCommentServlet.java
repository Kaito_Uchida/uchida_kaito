package uchida_kaito.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uchida_kaito.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })


public class DeleteCommentServlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
		int deleteCommentId = Integer.parseInt(request.getParameter("deleteCommentId"));

		 new CommentService().delete(deleteCommentId);
		 response.sendRedirect("./");
	}

}
