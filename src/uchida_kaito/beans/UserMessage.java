package uchida_kaito.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {

    private int id;
    private String account;
    private String title;
    private String category;
    private String text;
    private Date createdDate;
    private int userId;


    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    public String getAccount() {
    	return account;
    }

    public void setAccount(String account) {
    	this.account = account;
    }

    public String getTitle() {
    	return title;
    }

    public void setTitle(String title) {
    	this.title = title;
    }

    public String getCategory() {
    	return category;
    }

    public void setCategory(String category) {
    	this.category = category;
    }

    public int getUserId() {
    	return userId;
    }

    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text = text;
    }

    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
}