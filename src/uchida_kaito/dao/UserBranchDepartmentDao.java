package uchida_kaito.dao;

import static uchida_kaito.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import uchida_kaito.beans.UserBranchDepartment;
import uchida_kaito.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {
	public List<UserBranchDepartment> select(Connection connection) {
		 PreparedStatement ps = null;
		    try {
		    	StringBuilder sql = new StringBuilder();
		    	sql.append("SELECT ");
		    	sql.append("    users.id as id, ");
		    	sql.append("    users.account as account,");
		    	sql.append("    users.name as name,");
		    	sql.append("    users.branch_id as branch_id, ");
		    	sql.append("    users.department_id as department_id, ");
		    	sql.append("    users.created_date as created_date, ");
		    	sql.append("    users.updated_date as updated_date, ");
		    	sql.append("    users.is_stopped as is_stopped, ");
		    	sql.append("    branches.name as branch_name, ");
		    	sql.append("    departments.name as department_name ");
		    	sql.append("FROM users ");
		    	sql.append("INNER JOIN branches ");
		    	sql.append("ON users.branch_id = branches.id ");
		    	sql.append("INNER JOIN departments ");
		    	sql.append("ON users.department_id = departments.id ");
		    	sql.append("ORDER BY created_date DESC");

		    	ps = connection.prepareStatement(sql.toString());
		    	ResultSet rs = ps.executeQuery();

		    	List<UserBranchDepartment> users = toManegement(rs);
		    	return users;

		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(ps);
		    }
	}

	 private List<UserBranchDepartment> toManegement(ResultSet rs) throws SQLException {

	        List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
	        try {
	            while (rs.next()) {
	            	UserBranchDepartment user = new UserBranchDepartment();
	            	user.setId(rs.getInt("id"));
	            	user.setName(rs.getString("name"));
	            	user.setAccount(rs.getString("account"));
	            	user.setBranchId(rs.getInt("branch_id"));
	            	user.setDepartmentId(rs.getInt("department_id"));
	            	user.setCreatedDate(rs.getTimestamp("created_date"));
	            	user.setUpdatedDate(rs.getTimestamp("updated_date"));
	            	user.setBranchName(rs.getString("branch_name"));
	            	user.setDepartmentName(rs.getString("department_name"));
	            	user.setIsStopped(rs.getInt("is_stopped"));


	            	users.add(user);
	            }
	            return users;
	        } finally {
	            close(rs);
	        }
	    }
}
