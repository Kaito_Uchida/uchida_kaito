package uchida_kaito.service;

import static uchida_kaito.utils.CloseableUtil.*;
import static uchida_kaito.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uchida_kaito.beans.Department;
import uchida_kaito.dao.DepartmentDao;

public class DepartmentService {
	public List<Department> select() {
		Connection connection = null;
        try {
            connection = getConnection();
            List<Department> departments = new DepartmentDao().select(connection);
            commit(connection);

            return departments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
