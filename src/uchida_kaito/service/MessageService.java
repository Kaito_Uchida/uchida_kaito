package uchida_kaito.service;

import static uchida_kaito.utils.CloseableUtil.*;
import static uchida_kaito.utils.DBUtil.*;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import uchida_kaito.beans.Message;
import uchida_kaito.beans.UserMessage;
import uchida_kaito.dao.MessageDao;
import uchida_kaito.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String startDate, String endDate, String searchWord) throws UnsupportedEncodingException {

        Connection connection = null;
        String sqlStartDate;
        String sqlEndDate;
        String sqlSearchWord;

        try {
            connection = getConnection();
            //絞り込みの開始日が入力されているか判定
            if (StringUtils.isEmpty(startDate)) {
            	sqlStartDate = "2020-04-01 00:00:00";
            }else {
            	sqlStartDate = startDate + " 00:00:00";
            }

            //絞り込みの終了日が入力されているか判定
            if (StringUtils.isEmpty(endDate)) {
            	//現在時刻を取得し、フォーマットを変更
                Date currentDate = new Date();
                SimpleDateFormat newCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                sqlEndDate = newCurrentDate.format(currentDate);
            }else {
            	sqlEndDate = endDate + " 23:59:59";
            }
            //カテゴリー検索が入力されているか判定
            if(StringUtils.isEmpty(searchWord)) {
            	sqlSearchWord = "%_%";
            } else {
            	sqlSearchWord = "%" + searchWord + "%";
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, sqlStartDate , sqlEndDate, sqlSearchWord);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int deleteMessageId) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, deleteMessageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}