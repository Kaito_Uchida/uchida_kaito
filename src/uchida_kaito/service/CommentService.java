package uchida_kaito.service;

import static uchida_kaito.utils.CloseableUtil.*;
import static uchida_kaito.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uchida_kaito.beans.Comment;
import uchida_kaito.beans.UserComment;
import uchida_kaito.dao.CommentDao;
import uchida_kaito.dao.UserCommentDao;

public class CommentService {
	 public void insert(Comment comment) {

		 Connection connection = null;
		 try {
			 connection = getConnection();
			 new CommentDao().insert(connection, comment);
			 commit(connection);
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }

	 public List<UserComment> select() {

		 Connection connection = null;
		 try {
			 connection = getConnection();
			 List<UserComment> comments = new UserCommentDao().select(connection);
			 commit(connection);

			 return comments;
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }

	 public void delete(int deleteCommentId) {
		 Connection connection = null;
		 try {
			 connection = getConnection();
			 new CommentDao().delete(connection, deleteCommentId);
			 commit(connection);
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }
}
