package uchida_kaito.service;

import static uchida_kaito.utils.CloseableUtil.*;
import static uchida_kaito.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import uchida_kaito.beans.User;
import uchida_kaito.beans.UserBranchDepartment;
import uchida_kaito.dao.UserBranchDepartmentDao;
import uchida_kaito.dao.UserDao;
import uchida_kaito.utils.CipherUtil;

public class UserService {
	 public void insert(User user) {
		 Connection connection = null;
		 try {
			 // パスワード暗号化
			 String encPassword = CipherUtil.encrypt(user.getPassword());
			 user.setPassword(encPassword);

			 connection = getConnection();
			 new UserDao().insert(connection, user);
			 commit(connection);
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	}
	 public User select(String account, String password) {
		 Connection connection = null;
		 try {
			 // パスワード暗号化
			 String encPassword = CipherUtil.encrypt(password);
			 connection = getConnection();
			 User user = new UserDao().select(connection, account, encPassword);
			 commit(connection);

			 return user;
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }

	 public User select(String account) {
		 Connection connection = null;
		 try {
			 connection = getConnection();
			 User user = new UserDao().select(connection, account);
			 commit(connection);

			 return user;
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }

	 public User settingSelect(String userId) {
		 Connection connection = null;
		 try {
			 connection = getConnection();
			 User user = new UserDao().settingSelect(connection, userId);
			 commit(connection);

			 return user;
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }

	 public List<UserBranchDepartment> select() {
		 Connection connection = null;
		 try {
			 connection = getConnection();
			 List<UserBranchDepartment> users= new UserBranchDepartmentDao().select(connection);
			 commit(connection);

			 return users;
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }

	 }
	 public User select(int userId) {
		 Connection connection = null;
		 try {
			 connection = getConnection();
			 User user = new UserDao().select(connection, userId);
			 commit(connection);

			 return user;
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }

	 public void update(User user) {
		 Connection connection = null;
		 try {
			 // パスワード入力があった場合、パスワード暗号化
			 if (!StringUtils.isEmpty(user.getPassword())) {
				 String encPassword = CipherUtil.encrypt(user.getPassword());
				 user.setPassword(encPassword);
			 }
			 connection = getConnection();
			 new UserDao().update(connection, user);
			 commit(connection);
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }
	 public void stop(int stoppedUserId, int currentStatus) {
		 Connection connection = null;
		 try {
			 connection = getConnection();
			 new UserDao().stop(connection, stoppedUserId, currentStatus);
			 commit(connection);
		 } catch (RuntimeException e) {
			 rollback(connection);
			 throw e;
		 } catch (Error e) {
			 rollback(connection);
			 throw e;
		 } finally {
			 close(connection);
		 }
	 }
}
