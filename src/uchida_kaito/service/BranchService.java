package uchida_kaito.service;

import static uchida_kaito.utils.CloseableUtil.*;
import static uchida_kaito.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uchida_kaito.beans.Branch;
import uchida_kaito.dao.BranchDao;

public class BranchService {

    public List<Branch> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            List<Branch> branches = new BranchDao().select(connection);
            commit(connection);

            return branches;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
